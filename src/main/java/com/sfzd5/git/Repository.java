package com.sfzd5.git;

import java.util.Date;

public class Repository {
	private String name;

	private String mapping;

	private String description;

	private Date createDate;

	public Repository() {
	}

	public Repository(String name, String mapping, String description) {
		this.name = name;
		this.mapping = mapping;
		this.description = description;
		this.createDate = new Date();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMapping() {
		return mapping;
	}

	public void setMapping(String mapping) {
		this.mapping = mapping;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
