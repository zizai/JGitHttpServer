package com.sfzd5;

import com.sfzd5.git.Repository;

import java.util.List;

public class RepositoryList {
    public List<Repository> repositories;

    public boolean contains(Repository rep) {
        for(Repository r : repositories){
            if(r.getMapping().equals(rep.getMapping()))
                return true;
        }
        return false;
    }
}
