# JGitHttpServer

#### 介绍

这是利用jGit包制作的一个简易Git http服务器。可放在U盘里随身携带。

#### 使用说明

1.  打开软件，创建仓库后点开始启动Git http服务。
2.  仓库地址：http://127.0.0.1: + HttpServerPort + / + 仓库的映射名称
3.  无需密码验证，可以直接下拉或提交。
4.  服务端口和存储路径可在config.json文件中修改
